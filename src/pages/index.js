import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <h2>Hi</h2>
    <p>
      I'm Ben an Australian Software Developer working in Sydney. This page will
      contain some information on some things I'm working on currently, or any
      other interesting things I come across. As well as being a portfolio of
      sorts.
    </p>
    <p>It's pretty bare right now so stay tuned as I update the site! </p>
  </Layout>
)

export default IndexPage
